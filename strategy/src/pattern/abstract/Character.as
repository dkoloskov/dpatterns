package pattern.abstract {
	import pattern.interfaces.WeaponBehavior;

	public class Character {
		private var weapon:WeaponBehavior;

		public function Character() {
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function fight():void {
			if (weapon != null) {
				weapon.useWeapon();
			}
			else {
				throw (new Error("Abstract fight method"));
			}
		}

		// Set strategy
		public function setWeapon(value:WeaponBehavior):void {
			weapon = value;
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
