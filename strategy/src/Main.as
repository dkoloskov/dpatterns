package
{
	import flash.display.Sprite;

	import pattern.AxeBehavior;
	import pattern.KnifeBehavior;
	import pattern.Knight;
	import pattern.abstract.Character;


	public class Main extends Sprite
	{
		public function Main()
		{
			var knight:Character = new Knight();
			knight.fight();

			// Set strategy
			knight.setWeapon(new KnifeBehavior());
			knight.fight();

			// Set strategy
			knight.setWeapon(new AxeBehavior());
			knight.fight();
		}
	}
}
