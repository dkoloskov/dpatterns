package pattern
{
	public class InventorySingleton
	{
		private static var _instance:InventorySingleton;

		private var _money:int;

		public function InventorySingleton(key:Key)
		{
			trace("InventorySingleton created");
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public static function getInstance():InventorySingleton
		{
			if(_instance == null)
			{
				// Another ways to create instance (without Key Class) can be used
				_instance = new InventorySingleton(new Key());
			}

			return _instance;
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
		public function get money():int
		{
			return _money;
		}

		public function set money(value:int):void
		{
			_money = value;
		}
	}
}

class Key
{
}