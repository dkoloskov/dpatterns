/**
 * User: Dreams-Ultra
 * Date: 01.06.15
 * Time: 19:24
 */
package pattern.abstract
{
	/**
	 * Abstract class.
	 * This class can be omitted in such small architecture,
	 * but for decorating big classes, it could be helpful.
	 */
	public class TeaDecoratorIngredient extends Tea
	{
		/**
		 * Decorated object/item/entity
		 */
		protected var tea:Tea;

		public function TeaDecoratorIngredient(tea:Tea)
		{
			this.tea = tea;
		}

		//==============================================================================
		//{region							PUBLIC METHODS
		//} endregion PUBLIC METHODS ===================================================
		/**
		 * Method is not used, it only shows what we will decorate in subclasses
		 */
		override public function description():String
		{
			return "Tea ingredient abstract description";
		}

		//==============================================================================
		//{region						PRIVATE\PROTECTED METHODS
		//} endregion PRIVATE\PROTECTED METHODS ========================================

		//==============================================================================
		//{region							EVENTS HANDLERS
		//} endregion EVENTS HANDLERS ==================================================

		//==============================================================================
		//{region							GETTERS/SETTERS
		//} endregion GETTERS/SETTERS ==================================================
	}
}
