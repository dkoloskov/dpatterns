/**
 * User: Dreams-Ultra
 * Date: 01.06.15
 * Time: 19:29
 */
package pattern
{
	import pattern.abstract.Tea;
	import pattern.abstract.TeaDecoratorIngredient;


	public class Lemon extends TeaDecoratorIngredient
	{
		public function Lemon(tea:Tea)
		{
			super(tea);
		}

		//==============================================================================
		//{region							PUBLIC METHODS
		//} endregion PUBLIC METHODS ===================================================
		override public function description():String
		{
			return tea.description() + ", lemon";
		}

		//==============================================================================
		//{region						PRIVATE\PROTECTED METHODS
		//} endregion PRIVATE\PROTECTED METHODS ========================================

		//==============================================================================
		//{region							EVENTS HANDLERS
		//} endregion EVENTS HANDLERS ==================================================

		//==============================================================================
		//{region							GETTERS/SETTERS
		//} endregion GETTERS/SETTERS ==================================================
	}
}
