package
{

	import flash.display.Sprite;

	import pattern.BlackTea;
	import pattern.GreenTea;
	import pattern.Lemon;
	import pattern.Mint;
	import pattern.Sugar;
	import pattern.abstract.Tea;


	public class Main extends Sprite
	{
		public function Main()
		{
			var beverage1:Tea = new BlackTea();
			// Decoration
			beverage1 = new Sugar(beverage1);
			trace(beverage1.description());

			var beverage2:Tea = new GreenTea();
			// Decoration
			beverage2 = new Lemon(beverage2);
			beverage2 = new Mint(beverage2);
			trace(beverage2.description());
		}
	}
}
