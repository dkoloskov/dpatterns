package pattern
{
	import flash.utils.setTimeout;

	import pattern.interfaces.IAvatarImage;


	// Pattern class
	public class AvatarImageProxy implements IAvatarImage
	{
		private var avatarImage:IAvatarImage;

		public function AvatarImageProxy()
		{
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function drawImage():void
		{
			// State pattern can be implemented here
			if(avatarImage == null)
			{
				drawMockupImage();

				// 1. Don't know, is subject (avatarImage) must be created in proxy, or passed to proxy as link.
				// 2. Also don't know, does subject (avatarImage) must be created in constructor (at the start),
				// or it should be created at first call of subject (avatarImage).
				downloadAvatarImage();
			}
			else
			{
				avatarImage.drawImage();
			}
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		private function downloadAvatarImage():void
		{
			setTimeout(avatarImageDownloadedHandler, 2000);
		}

		private function drawMockupImage():void
		{
			trace("Mockup image drawn.");
		}

		private function deleteMockupImage():void
		{
			trace("Mockup image deleted.");
		}
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		private function avatarImageDownloadedHandler():void
		{
			trace("===============================================");
			deleteMockupImage();
			// Here we can pass some image to "AvatarImage" constructor
			avatarImage = new AvatarImage();

			drawImage();
		}

		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
