package
{

	import flash.display.Sprite;
	import flash.text.TextField;

	import pattern.AvatarImageProxy;
	import pattern.interfaces.IAvatarImage;


	public class Main extends Sprite
	{
		public function Main()
		{
			// Creation of proxy can be moved to factory (one of Factory pattern realizations)
			var avatarImage:IAvatarImage = new AvatarImageProxy();

			// Need wait for few seconds to see result
			avatarImage.drawImage();
		}
	}
}
