package pattern
{
	import pattern.interfaces.IDragon;


	// Protection proxy
	public class DragonProxyDragonAccess implements IDragon
	{
		private var dragon:IDragon;

		public function DragonProxyDragonAccess(dragon:IDragon)
		{
			this.dragon = dragon;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function fireBreath():void
		{
			dragon.fireBreath();
		}

		public function fly():void
		{
			dragon.fly();
		}

		public function slayDragon():void
		{
			throw new Error("Illegal method call! Dragon cannot access this method.");
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
