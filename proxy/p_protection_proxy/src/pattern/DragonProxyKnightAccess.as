package pattern
{
	import pattern.interfaces.IDragon;


	// Protection proxy
	public class DragonProxyKnightAccess implements IDragon
	{
		private var dragon:IDragon;

		public function DragonProxyKnightAccess(dragon:IDragon)
		{
			this.dragon = dragon;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function fireBreath():void
		{
			throw new Error("Illegal method call! Knight cannot access this method.");
		}

		public function fly():void
		{
			throw new Error("Illegal method call! Knight cannot access this method.");
		}

		public function slayDragon():void
		{
			dragon.slayDragon();
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
