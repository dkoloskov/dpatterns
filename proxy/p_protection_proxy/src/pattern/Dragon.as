package pattern
{
	import pattern.interfaces.*;


	public class Dragon implements IDragon
	{
		public function Dragon()
		{
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function fireBreath():void
		{
			trace("Fire breath. 'Shshshshshsh'");
		}

		public function fly():void
		{
			trace("Fly high. 'Whoosh, whoosh'");
		}

		public function slayDragon():void
		{
			trace("Dragon slayed. 'Grrrrr...' ");
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
