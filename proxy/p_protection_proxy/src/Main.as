package
{
	import flash.display.Sprite;

	import pattern.Dragon;
	import pattern.DragonProxyDragonAccess;
	import pattern.DragonProxyKnightAccess;
	import pattern.interfaces.IDragon;


	public class Main extends Sprite
	{
		public function Main()
		{
			// Maybe this is not good example, but it should give basic understanding of
			// current type of Proxy pattern realization.
			var dragon:IDragon = new Dragon();

			// 1. Proxy should be created by Factory.
			// 2. Also, control (call of methods) could be provided by special classes - Handlers.
			// Handlers are contained by Proxy.
			var dragonAccess:DragonProxyDragonAccess = new DragonProxyDragonAccess(dragon);
			trace("=================== DRAGON ACCESS ===================");
			doDragonStuff(dragonAccess);

			var knightAccess:DragonProxyKnightAccess = new DragonProxyKnightAccess(dragon);
			trace("=================== KNIGHT ACCESS ===================");
			doDragonStuff(knightAccess);
		}

		private function doDragonStuff(dragon:IDragon):void
		{
			var methods:Vector.<Function> = new <Function>[dragon.fireBreath, dragon.fly, dragon.slayDragon];

			var i:uint;
			var len:uint = methods.length;
			var method:Function;
			for(i=0; i<len; i++)
			{
				method = methods[i];

				try
				{
					method();
				}
				catch(e:Error)
				{
					trace(e.message);
				}
			}
		}
	}
}
