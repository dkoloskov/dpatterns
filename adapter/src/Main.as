package
{

	import flash.display.Sprite;

	import pattern.Platypus;

	import pattern.PlatypusDuckAdapter;

	import pattern.interfaces.IDuck;
	import pattern.interfaces.IPlatypus;


	public class Main extends Sprite
	{
		public function Main()
		{
			var platypus:IPlatypus = new Platypus();

			// Pattern
			var duck:IDuck = new PlatypusDuckAdapter(platypus);
			duck.quack();
			duck.fly();
		}
	}
}
