package pattern
{
	import pattern.interfaces.IDuck;
	import pattern.interfaces.IPlatypus;


	public class PlatypusDuckAdapter implements IDuck
	{
		private var platypus:IPlatypus;

		public function PlatypusDuckAdapter(platypus:IPlatypus)
		{
			this.platypus = platypus;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
		public function quack():void
		{
			platypus.roar();
		}

		public function fly():void
		{
			var i:uint;
			for(i=0; i<3; i++)
			{
				platypus.jump();
			}
		}
	}
}
