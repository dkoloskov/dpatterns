package
{

	import flash.display.Sprite;

	import pattern.CeilingFan;

	import pattern.Light;

	import pattern.RemoteControlWithUndo;
	import pattern.commands.CeilingFanSetSpeedHighCommand;
	import pattern.commands.CeilingFanSetSpeedLowCommand;
	import pattern.commands.CeilingFanSetSpeedMediumCommand;
	import pattern.commands.CeilingFanTurnOffCommand;
	import pattern.commands.LightOffCommand;
	import pattern.commands.LightOnCommand;
	import pattern.commands.macro.MacroCommand;
	import pattern.interfaces.ICommand;


	// Pattern realized with "undo" functionality and macro command example
	public class Main extends Sprite
	{
		public function Main()
		{
			// =============== INITIALIZATION ===============
			var remoteControl:RemoteControlWithUndo = new RemoteControlWithUndo();

			var light:Light = new Light();
			var ceilingFan:CeilingFan = new CeilingFan();

			// Creation commands
			var lightOnCommand:ICommand = new LightOnCommand(light);
			var lightOffCommand:ICommand = new LightOffCommand(light);

			var ceilingFanSetSpeedLowCommand:ICommand = new CeilingFanSetSpeedLowCommand(ceilingFan);
			var ceilingFanSetSpeedMediumCommand:ICommand = new CeilingFanSetSpeedMediumCommand(ceilingFan);
			var ceilingFanSetSpeedHighCommand:ICommand = new CeilingFanSetSpeedHighCommand(ceilingFan);
			var ceilingFanTurnOffCommand:ICommand = new CeilingFanTurnOffCommand(ceilingFan);

			// Creation of macro command
			var turnOnAll:ICommand = new MacroCommand(new <ICommand>[lightOnCommand, ceilingFanSetSpeedMediumCommand]);
			var turnOffAll:ICommand = new MacroCommand(new <ICommand>[lightOffCommand, ceilingFanTurnOffCommand]);

			// Set commands
			remoteControl.setCommand(0, lightOnCommand, lightOffCommand);

			remoteControl.setCommand(1, ceilingFanSetSpeedLowCommand, ceilingFanTurnOffCommand);
			remoteControl.setCommand(2, ceilingFanSetSpeedMediumCommand, ceilingFanTurnOffCommand);
			remoteControl.setCommand(3, ceilingFanSetSpeedHighCommand, ceilingFanTurnOffCommand);

			remoteControl.setCommand(4, turnOnAll, turnOffAll);

			// =============== TESTING ===============
			remoteControl.onButtonPushed(0);
			remoteControl.undoButtonPushed();

			remoteControl.onButtonPushed(1);
			remoteControl.onButtonPushed(3);
			remoteControl.undoButtonPushed();
			remoteControl.offButtonPushed(1);
			trace("-------------------------------");
			trace("-------------------------------");

			remoteControl.onButtonPushed(4);
			remoteControl.offButtonPushed(4);
			remoteControl.undoButtonPushed();
			trace("-------------------------------");
			trace("-------------------------------");

			trace(remoteControl.toString());
		}
	}
}
