package pattern.interfaces
{
	public interface ICommand
	{
		//--------------------------------------------------------------------------
		//   							METHODS
		//--------------------------------------------------------------------------
		function execute():void;

		// Undo method can be realized in Main pattern
		// But it is optional
		function undo():void;
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
