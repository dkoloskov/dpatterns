package pattern
{
	public class CeilingFan
	{
		static public const TURNED_OFF:uint = 0;
		static public const SPEED_LOW:uint = 1;
		static public const SPEED_MEDIUM:uint = 2;
		static public const SPEED_HIGH:uint = 3;

		private var _speed:uint;

		public function CeilingFan()
		{
			_speed = TURNED_OFF;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function setSpeedLow():void
		{
			_speed = SPEED_LOW;
			trace("fan speed - low");
		}

		public function setSpeedMedium():void
		{
			_speed = SPEED_MEDIUM;
			trace("fan speed - medium");
		}

		public function setSpeedHigh():void
		{
			_speed = SPEED_HIGH;
			trace("fan speed - high");
		}

		// setSpeedOff - not best name for method, it was fast decision
		public function turnOff():void
		{
			_speed = TURNED_OFF;
			trace("fan speed - turned off");
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
		public function get speed():uint
		{
			return _speed;
		}
	}
}
