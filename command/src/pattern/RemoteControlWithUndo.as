package pattern
{
	import avmplus.getQualifiedClassName;

	import pattern.interfaces.ICommand;


	// Initiator
	// Actually code can be much simpler without "undo" functionality
	// Also here can be organize queue... Don't have enough knowledge right now
	public class RemoteControlWithUndo
	{
		private var onCommands:Array;
		private var offCommands:Array;

		private var lastComand:ICommand;

		public function RemoteControlWithUndo()
		{
			onCommands = [];
			offCommands = [];
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function setCommand(slot:int, onCommand:ICommand, offCommand:ICommand):void
		{
			onCommands[slot] = onCommand;
			offCommands[slot] = offCommand;
		}

		public function onButtonPushed(slot:int):void
		{
			if(onCommands[slot] != null)
			{
				var command:ICommand = onCommands[slot];
				command.execute();

				lastComand = command;
			}
		}

		public function offButtonPushed(slot:int):void
		{
			if(offCommands[slot] != null)
			{
				var command:ICommand = offCommands[slot];
				command.execute();

				lastComand = command;
			}
		}

		public function undoButtonPushed():void
		{
			if(lastComand != null)
			{
				lastComand.undo();
			}
		}

		// This method only for tests
		public function toString():String
		{
			var s:String = "";
			var command:ICommand;

			var i:uint;
			var len:uint = onCommands.length;
			for(i = 0; i < len; i++)
			{
				if(onCommands[i] != null)
				{
					command = onCommands[i];
					s += getQualifiedClassName(command) + "\n";

					command = offCommands[i];
					s += getQualifiedClassName(command) + "\n";
				}
			}

			return s;
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
