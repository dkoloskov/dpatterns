package pattern.commands.macro
{
	import pattern.interfaces.ICommand;


	public class MacroCommand implements ICommand
	{
		private var commands:Vector.<ICommand>;

		public function MacroCommand(commands:Vector.<ICommand>)
		{
			this.commands = commands;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function execute():void
		{
			var i:uint;
			var len:uint = commands.length;
			var command:ICommand;
			for(i=0; i<len; i++)
			{
				command = commands[i];
				command.execute();
			}
		}

		public function undo():void
		{
			var i:uint;
			var len:uint = commands.length;
			var command:ICommand;
			for(i=0; i<len; i++)
			{
				command = commands[i];
				command.undo();
			}
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
