package pattern.commands
{
	import pattern.CeilingFan;
	import pattern.abstract.CeilingFanSetSpeedAbstractCommand;


	public class CeilingFanSetSpeedHighCommand extends CeilingFanSetSpeedAbstractCommand
	{
		public function CeilingFanSetSpeedHighCommand(ceilingFan:CeilingFan)
		{
			commandSpeed = CeilingFan.SPEED_HIGH;
			super(ceilingFan);
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
