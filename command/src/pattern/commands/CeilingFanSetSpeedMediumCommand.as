package pattern.commands
{
	import pattern.CeilingFan;
	import pattern.abstract.CeilingFanSetSpeedAbstractCommand;


	public class CeilingFanSetSpeedMediumCommand extends CeilingFanSetSpeedAbstractCommand
	{
		public function CeilingFanSetSpeedMediumCommand(ceilingFan:CeilingFan)
		{
			commandSpeed = CeilingFan.SPEED_MEDIUM;
			super(ceilingFan);
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
