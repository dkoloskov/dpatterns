package pattern.commands
{
	import pattern.CeilingFan;
	import pattern.abstract.CeilingFanSetSpeedAbstractCommand;


	public class CeilingFanTurnOffCommand extends CeilingFanSetSpeedAbstractCommand
	{
		public function CeilingFanTurnOffCommand(ceilingFan:CeilingFan)
		{
			commandSpeed = CeilingFan.TURNED_OFF;
			super(ceilingFan);
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
