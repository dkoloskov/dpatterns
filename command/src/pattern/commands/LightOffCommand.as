package pattern.commands
{
	import pattern.Light;
	import pattern.interfaces.ICommand;


	public class LightOffCommand implements ICommand
	{
		private var light:Light;

		public function LightOffCommand(light:Light)
		{
			this.light = light;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function execute():void
		{
			light.lightOff();
		}

		public function undo():void
		{
			light.lightOn();
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
