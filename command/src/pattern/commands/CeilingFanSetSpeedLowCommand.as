package pattern.commands
{
	import pattern.CeilingFan;
	import pattern.abstract.CeilingFanSetSpeedAbstractCommand;


	public class CeilingFanSetSpeedLowCommand extends CeilingFanSetSpeedAbstractCommand
	{
		public function CeilingFanSetSpeedLowCommand(ceilingFan:CeilingFan)
		{
			commandSpeed = CeilingFan.SPEED_LOW;
			super(ceilingFan);
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
