package pattern.commands
{
	import pattern.Light;
	import pattern.interfaces.ICommand;


	public class LightOnCommand implements ICommand
	{
		private var light:Light;

		public function LightOnCommand(light:Light)
		{
			this.light = light;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function execute():void
		{
			light.lightOn();
		}

		public function undo():void
		{
			light.lightOff();
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
