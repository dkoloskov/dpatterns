package pattern.abstract
{
	import pattern.CeilingFan;
	import pattern.interfaces.ICommand;


	// Abstract Class, shouldn't be used directly
	public class CeilingFanSetSpeedAbstractCommand implements ICommand
	{
		private var ceilingFan:CeilingFan;

		protected var commandSpeed:uint;
		private var previousSpeed:uint;

		public function CeilingFanSetSpeedAbstractCommand(ceilingFan:CeilingFan)
		{
			this.ceilingFan = ceilingFan;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function execute():void
		{
			previousSpeed = ceilingFan.speed;
			setSpeed(commandSpeed);
		}

		public function undo():void
		{
			setSpeed(previousSpeed);
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		private function setSpeed(speed:uint):void
		{
			switch(speed)
			{
				case CeilingFan.SPEED_LOW:
					ceilingFan.setSpeedLow();
					break;

				case CeilingFan.SPEED_MEDIUM:
					ceilingFan.setSpeedMedium();
					break;

				case CeilingFan.SPEED_HIGH:
					ceilingFan.setSpeedHigh();
					break;

				case CeilingFan.TURNED_OFF:
					ceilingFan.turnOff();
					break;
			}
		}

		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
