package
{
	import flash.display.Sprite;

	import pattern.Bag;
	import pattern.Item;
	import pattern.interfaces.IComponent;


	public class Main extends Sprite
	{
		public function Main()
		{
			// Bag 1
			var mainBag:IComponent = new Bag("mainBag: big bag");

			var mainBag_item1:IComponent = new Item("item1: sword");
			mainBag.addComponent(mainBag_item1);

			var mainBag_item2:IComponent = new Item("item2: axe");
			mainBag.addComponent(mainBag_item2);

			var bag2:IComponent = new Bag("bag2: small bag");
			mainBag.addComponent(bag2);

			var mainBag_item4:IComponent = new Item("item4: fireball scroll");
			mainBag.addComponent(mainBag_item4);

			// Bag 2
			var bag2_item1:IComponent = new Item("item1: health potion");
			bag2.addComponent(bag2_item1);

			var bag2_item2:IComponent = new Item("item2: poison potion");
			bag2.addComponent(bag2_item2);

			// Pattern test
			mainBag.display();

			trace("===================================================");

			// Pattern test 2
			mainBag.removeComponent(bag2_item2);
			mainBag.display();

			trace("===================================================");

			// Pattern test 3
			mainBag.removeComponent(bag2);
			mainBag.display();
		}
	}
}