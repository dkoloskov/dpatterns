package pattern.interfaces
{
	public interface IComponent
	{
		//--------------------------------------------------------------------------
		//   							METHODS
		//--------------------------------------------------------------------------
		function addComponent(component:IComponent):void;

		function removeComponent(component:IComponent):void;

		// indent here only for making test looks better
		function display(indent:String  = ""):void;

		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
		function get name():String;
	}
}
