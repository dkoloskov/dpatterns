package pattern
{
	import pattern.abstract.Component;
	import pattern.interfaces.IComponent;


	// Composite/Node class (Composite pattern)
	public class Bag extends Component
	{
		private var items:Vector.<IComponent>;

		public function Bag(name:String)
		{
			_name = name;

			items = new <IComponent>[];
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		override public function addComponent(component:IComponent):void
		{
			items.push(component);
		}

		// Not shore about removing component in sub-components
		override public function removeComponent(component:IComponent):void
		{
			var indexOfComponent:int = items.indexOf(component);
			if(indexOfComponent != -1)
			{
				items.splice(indexOfComponent, 1);
			}
			else
			{
				var i:uint;
				var len:uint = items.length;
				var c:IComponent;
				for(i = 0; i < len; i++)
				{
					c = items[i];
					try
					{
						c.removeComponent(component);
					}
					catch(error:Error)
					{
						// Catch error for leaf components
					}
				}
			}
		}

		override public function display(indent:String = ""):void
		{
			trace(indent + "-=" + _name + "=-");

			var i:uint;
			var len:uint = items.length;
			var c:IComponent;
			for(i = 0; i < len; i++)
			{
				c = items[i];
				c.display(indent+"   ");
			}
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
