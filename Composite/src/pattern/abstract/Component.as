package pattern.abstract
{
	import pattern.interfaces.IComponent;


	// Abstract class
	public class Component implements IComponent
	{
		protected var _name:String;

		public function Component()
		{
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function addComponent(component:IComponent):void
		{
			throw (new Error("Unsupported method"));
		}

		public function removeComponent(component:IComponent):void
		{
			throw (new Error("Unsupported method"));
		}

		public function display(indent:String  = ""):void
		{
			throw (new Error("Unsupported method"));
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
		public function get name():String
		{
			return _name;
		}
	}
}
