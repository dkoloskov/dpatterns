package
{

	import flash.display.Sprite;

	import pattern.Businessman;
	import pattern.CrazyHomeless;

	import pattern.Newspaper;
	import pattern.abstract.Citizen;


	public class Main extends Sprite
	{
		public function Main()
		{
			var newspaper:Newspaper = new Newspaper();

			var businessman:Citizen = new Businessman();
			var crazeHomeless:Citizen = new CrazyHomeless();

			newspaper.addObserver(businessman);
			newspaper.addObserver(crazeHomeless);

			newspaper.publishNewspaper();
			newspaper.removeObserver(businessman);
			newspaper.publishNewspaper();
			newspaper.publishNewspaper();
		}
	}
}
