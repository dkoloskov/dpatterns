/**
 * User: Dreams-Ultra
 * Date: 01.06.15
 * Time: 17:04
 */
package pattern.abstract
{
	import pattern.interfaces.IObserver;


	public class Citizen implements IObserver
	{
		public function Citizen()
		{
		}

		//==============================================================================
		//{region							PUBLIC METHODS
		//} endregion PUBLIC METHODS ===================================================
		public function update():void
		{
			trace("Abstract method update");
		}

		//==============================================================================
		//{region						PRIVATE\PROTECTED METHODS
		//} endregion PRIVATE\PROTECTED METHODS ========================================

		//==============================================================================
		//{region							EVENTS HANDLERS
		//} endregion EVENTS HANDLERS ==================================================

		//==============================================================================
		//{region							GETTERS/SETTERS
		//} endregion GETTERS/SETTERS ==================================================
	}
}
