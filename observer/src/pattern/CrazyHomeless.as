/**
 * User: Dreams-Ultra
 * Date: 01.06.15
 * Time: 17:06
 */
package pattern
{
	import pattern.abstract.Citizen;


	public class CrazyHomeless extends Citizen
	{
		var NEWSPAPERS_NUM_FOR_HOUSE:uint = 3;
		var newspapersCount:uint;

		public function CrazyHomeless()
		{
			newspapersCount = 0;
			super();
		}

		//==============================================================================
		//{region							PUBLIC METHODS
		//} endregion PUBLIC METHODS ===================================================
		override public function update():void
		{
			newspapersCount++;
			trace("CrazyHomeless: got newspaper");
			if(newspapersCount == NEWSPAPERS_NUM_FOR_HOUSE)
			{
				trace("CrazyHomeless: built a house of newspapers");
			}
		}

		//==============================================================================
		//{region						PRIVATE\PROTECTED METHODS
		//} endregion PRIVATE\PROTECTED METHODS ========================================

		//==============================================================================
		//{region							EVENTS HANDLERS
		//} endregion EVENTS HANDLERS ==================================================

		//==============================================================================
		//{region							GETTERS/SETTERS
		//} endregion GETTERS/SETTERS ==================================================
	}
}
