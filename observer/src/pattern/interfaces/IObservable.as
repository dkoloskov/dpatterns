package pattern.interfaces
{
	public interface IObservable
	{
		//==============================================================================
		//{region							METHODS
		//} endregion METHODS ==========================================================
		/**
		 * Register observer
		 */
		function addObserver(o:IObserver):void

		function removeObserver(o:IObserver):void

		function notifyObservers():void

		//==============================================================================
		//{region							GETTERS/SETTERS
		//} endregion GETTERS/SETTERS ==================================================
	}
}
