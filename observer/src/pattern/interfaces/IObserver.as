package pattern.interfaces
{
	public interface IObserver
	{
		//==============================================================================
		//{region							METHODS
		//} endregion METHODS ==========================================================
		function update():void

		//==============================================================================
		//{region							GETTERS/SETTERS
		//} endregion GETTERS/SETTERS ==================================================
	}
}
