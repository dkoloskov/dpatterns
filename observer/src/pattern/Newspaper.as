package pattern
{
	import pattern.interfaces.IObservable;
	import pattern.interfaces.IObserver;


	public class Newspaper implements IObservable
	{
		private var observers:Vector.<IObserver>;

		public function Newspaper()
		{
			observers = new <IObserver>[];
		}

		//==============================================================================
		//{region							PUBLIC METHODS
		//} endregion PUBLIC METHODS ===================================================
		public function publishNewspaper():void
		{
			trace("Newspaper: publish newspaper");
			notifyObservers();
		}

		//==============================================================================
		//{region						PRIVATE\PROTECTED METHODS
		//} endregion PRIVATE\PROTECTED METHODS ========================================

		//==============================================================================
		//{region							EVENTS HANDLERS
		//} endregion EVENTS HANDLERS ==================================================

		//==============================================================================
		//{region							GETTERS/SETTERS
		//} endregion GETTERS/SETTERS ==================================================
		public function addObserver(o:IObserver):void
		{
			observers.push(o);
		}

		public function removeObserver(o:IObserver):void
		{
			var indexOfObserver:Number = observers.indexOf(o);
			observers.splice(indexOfObserver, 1);
		}

		public function notifyObservers():void
		{
			var i:uint;
			var len:uint = observers.length;
			var o:IObserver;
			for(i = 0; i < len; i++)
			{
				o = observers[i];
				o.update();
			}
		}
	}
}
