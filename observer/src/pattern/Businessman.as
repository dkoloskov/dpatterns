/**
 * User: Dreams-Ultra
 * Date: 01.06.15
 * Time: 17:06
 */
package pattern
{
	import pattern.abstract.Citizen;


	public class Businessman extends Citizen
	{
		public function Businessman()
		{
			super();
		}

		//==============================================================================
		//{region							PUBLIC METHODS
		//} endregion PUBLIC METHODS ===================================================
		override public function update():void
		{
			trace("Businessman: got newspaper");
		}

		//==============================================================================
		//{region						PRIVATE\PROTECTED METHODS
		//} endregion PRIVATE\PROTECTED METHODS ========================================

		//==============================================================================
		//{region							EVENTS HANDLERS
		//} endregion EVENTS HANDLERS ==================================================

		//==============================================================================
		//{region							GETTERS/SETTERS
		//} endregion GETTERS/SETTERS ==================================================
	}
}
