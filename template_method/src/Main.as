package
{

	import flash.display.Sprite;

	import pattern.ArmorCraft;
	import pattern.HealthPotionCraft;
	import pattern.PoisonPotionCraft;
	import pattern.abstract.ItemCraft;


	public class Main extends Sprite
	{
		public function Main()
		{
			var itemCraft1:ItemCraft = new ArmorCraft();
			itemCraft1.create();
			trace("==============================================");
			var itemCraft2:ItemCraft = new HealthPotionCraft();
			itemCraft2.create();
			trace("==============================================");

			// Example of hook method work
			var itemCraft3:ItemCraft = new PoisonPotionCraft(false);
			itemCraft3.create();
			trace("==============================================");
			var itemCraft4:ItemCraft = new PoisonPotionCraft(true);
			itemCraft4.create();
			trace("==============================================");
		}
	}
}
