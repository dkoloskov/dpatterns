package pattern.abstract
{
	public class ItemCraft
	{
		public function ItemCraft()
		{
		}

		//==============================================================================
		//{region							PUBLIC METHODS
		//} endregion PUBLIC METHODS ===================================================
		// Template method
		final public function create():void
		{
			// Abstract method
			setIngredients();
			// Concrete method
			make();

			// Hook method
			if(itemShouldBeUsed())
			{
				// Abstract method
				useItem();
			}
		}

		//==============================================================================
		//{region						PRIVATE\PROTECTED METHODS
		//} endregion PRIVATE\PROTECTED METHODS ========================================
		// Abstract method, should be extended/implemented in subclasses
		protected function setIngredients():void
		{
			throw new Error("Abstract method called!");
		}

		// Abstract method, should be extended/implemented in subclasses
		protected function useItem():void
		{
			throw new Error("Abstract method called!");
		}

		// Hook method
		protected function itemShouldBeUsed():Boolean
		{
			return true;
		}

		// Concrete method
		private function make():void
		{
			trace("Making item... Item created.");
		}

		//==============================================================================
		//{region							EVENTS HANDLERS
		//} endregion EVENTS HANDLERS ==================================================

		//==============================================================================
		//{region							GETTERS/SETTERS
		//} endregion GETTERS/SETTERS ==================================================
	}
}
