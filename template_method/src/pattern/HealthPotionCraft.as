package pattern
{
	import pattern.abstract.*;
	public class HealthPotionCraft extends ItemCraft
	{
		public function HealthPotionCraft()
		{
		}

		//==============================================================================
		//{region							PUBLIC METHODS
		//} endregion PUBLIC METHODS ===================================================

		//==============================================================================
		//{region						PRIVATE\PROTECTED METHODS
		//} endregion PRIVATE\PROTECTED METHODS ========================================
		override protected function setIngredients():void
		{
			trace("Set ingredients: Herbs, Magic.");
		}

		override protected function useItem():void
		{
			trace("Use item: Health +10");
		}

		//==============================================================================
		//{region							EVENTS HANDLERS
		//} endregion EVENTS HANDLERS ==================================================

		//==============================================================================
		//{region							GETTERS/SETTERS
		//} endregion GETTERS/SETTERS ==================================================
	}
}
