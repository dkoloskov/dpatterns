package pattern
{
	import pattern.abstract.*;
	public class ArmorCraft extends ItemCraft
	{
		public function ArmorCraft()
		{
		}

		//==============================================================================
		//{region							PUBLIC METHODS
		//} endregion PUBLIC METHODS ===================================================

		//==============================================================================
		//{region						PRIVATE\PROTECTED METHODS
		//} endregion PRIVATE\PROTECTED METHODS ========================================
		override protected function setIngredients():void
		{
			trace("Set ingredients: Iron, Steel, Leather.");
		}

		override protected function useItem():void
		{
			trace("Use item: Defence +10");
		}

		//==============================================================================
		//{region							EVENTS HANDLERS
		//} endregion EVENTS HANDLERS ==================================================

		//==============================================================================
		//{region							GETTERS/SETTERS
		//} endregion GETTERS/SETTERS ==================================================
	}
}
