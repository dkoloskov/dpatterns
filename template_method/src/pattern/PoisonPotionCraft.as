package pattern
{
	import pattern.abstract.*;


	public class PoisonPotionCraft extends ItemCraft
	{
		private var shouldBeUsed:Boolean;

		public function PoisonPotionCraft(shouldBeUsed:Boolean)
		{
			this.shouldBeUsed = shouldBeUsed;
		}

		//==============================================================================
		//{region							PUBLIC METHODS
		//} endregion PUBLIC METHODS ===================================================

		//==============================================================================
		//{region						PRIVATE\PROTECTED METHODS
		//} endregion PRIVATE\PROTECTED METHODS ========================================
		override protected function setIngredients():void
		{
			trace("Set ingredients: Bad herbs, Magic.");
		}

		override protected function useItem():void
		{
			trace("Use item: You are dead.");
		}

		override protected function itemShouldBeUsed():Boolean
		{
			return shouldBeUsed;
		}

		//==============================================================================
		//{region							EVENTS HANDLERS
		//} endregion EVENTS HANDLERS ==================================================

		//==============================================================================
		//{region							GETTERS/SETTERS
		//} endregion GETTERS/SETTERS ==================================================
	}
}
