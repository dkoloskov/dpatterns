package
{

	import flash.display.Sprite;

	import pattern.AirConditioning;
	import pattern.Light;
	import pattern.RoomFacade;
	import pattern.TV;
	import pattern.interfaces.IAirConditioning;
	import pattern.interfaces.ILight;
	import pattern.interfaces.IRoomFacade;
	import pattern.interfaces.ITV;


	public class Main extends Sprite
	{
		public function Main()
		{
			var light:ILight = new Light();
			var tv:ITV = new TV();
			var airConditioning:IAirConditioning = new AirConditioning();

			// Pattern
			var roomFacade:IRoomFacade = new RoomFacade(light, tv, airConditioning);
			roomFacade.enableRelaxMode();
			roomFacade.disableRelaxMode();
		}
	}
}
