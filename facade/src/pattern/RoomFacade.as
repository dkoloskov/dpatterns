package pattern
{
	import pattern.interfaces.IAirConditioning;
	import pattern.interfaces.IRoomFacade;
	import pattern.interfaces.ILight;
	import pattern.interfaces.ITV;


	public class RoomFacade implements IRoomFacade
	{
		private var light:ILight;
		private var tv:ITV;
		private var airConditioning:IAirConditioning;

		public function RoomFacade(light:ILight, tv:ITV, airConditioning:IAirConditioning)
		{
			this.light = light;
			this.tv = tv;
			this.airConditioning = airConditioning;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function enableRelaxMode():void
		{
			light.lightOn();
			tv.turnOn();
			airConditioning.turnOn();
		}

		public function disableRelaxMode():void
		{
			airConditioning.turnOff();
			tv.turnOff();
			light.lightOff();
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
