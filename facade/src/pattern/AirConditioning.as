package pattern
{
	import pattern.interfaces.IAirConditioning;


	public class AirConditioning implements IAirConditioning
	{
		public function AirConditioning()
		{
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function turnOn():void
		{
			trace("Air conditioning on");
		}

		public function turnOff():void
		{
			trace("Air conditioning off");
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
