package
{

	import flash.display.Sprite;

	import pattern.DefenseSpellbook;
	import pattern.OffenseSpellbook;
	import pattern.interfaces.ISpellbook;


	public class Main extends Sprite
	{
		public function Main()
		{
			var spellbook1:ISpellbook = new OffenseSpellbook();
			trace(spellbook1.createFireSpell().name);
			trace(spellbook1.createWaterSpell().name);

			var spellbook2:ISpellbook = new DefenseSpellbook();
			trace(spellbook2.createFireSpell().name);
			trace(spellbook2.createWaterSpell().name);
		}
	}
}
