package pattern
{
	import pattern.interfaces.IFireSpell;
	import pattern.interfaces.ISpellbook;
	import pattern.interfaces.IWaterSpell;


	public class OffenseSpellbook implements ISpellbook
	{
		public function OffenseSpellbook()
		{
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function createFireSpell():IFireSpell
		{
			return new Fireball();
		}

		public function createWaterSpell():IWaterSpell
		{
			return new IceShards();
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
