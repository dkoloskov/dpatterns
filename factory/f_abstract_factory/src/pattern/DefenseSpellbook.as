package pattern
{
	import pattern.interfaces.IFireSpell;
	import pattern.interfaces.ISpellbook;
	import pattern.interfaces.IWaterSpell;


	public class DefenseSpellbook implements ISpellbook
	{
		public function DefenseSpellbook()
		{
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function createFireSpell():IFireSpell
		{
			return new FireShield();
		}

		public function createWaterSpell():IWaterSpell
		{
			return new Heal();
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
