package
{

	import flash.display.Sprite;

	import pattern.ItemsEnum;
	import pattern.TraderSimpleFactory;
	import pattern.interfaces.IItem;


	public class Main extends Sprite
	{
		public function Main()
		{
			// buyItem - is really a create item method
			var item1:IItem = TraderSimpleFactory.buyItem(ItemsEnum.SWORD);
			trace(item1.name);

			var item2:IItem = TraderSimpleFactory.buyItem(ItemsEnum.MAGIC_WAND);
			trace(item2.name);

			var item3:IItem = TraderSimpleFactory.buyItem(ItemsEnum.CARROT);
			trace(item3.name);
		}
	}
}
