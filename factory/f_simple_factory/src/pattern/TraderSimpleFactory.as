package pattern
{
	import pattern.interfaces.IItem;


	public class TraderSimpleFactory
	{
		public function TraderSimpleFactory()
		{
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		// buyItem - is really a create item method
		public static function buyItem(itemName:String):IItem
		{
			var item:IItem;

			switch(itemName)
			{
				case ItemsEnum.SWORD:
					item = new Sword();
					break;

				case ItemsEnum.MAGIC_WAND:
					item = new MagicWand();
					break;

				case ItemsEnum.CARROT:
					item = new Carrot();
					break;
			}

			return item;
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
