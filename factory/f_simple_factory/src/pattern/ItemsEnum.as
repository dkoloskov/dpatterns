package pattern
{
	public class ItemsEnum
	{
		public static const SWORD:String = "SWORD";
		public static const MAGIC_WAND:String = "MAGIC_WAND";
		public static const CARROT:String = "CARROT";
	}
}
