package pattern
{
	import pattern.abstract.Item;


	public class Sword extends Item
	{
		public function Sword()
		{
			_name = "Sword";
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
