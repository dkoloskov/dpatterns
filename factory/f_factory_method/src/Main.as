package
{
	import flash.display.Sprite;

	import pattern.CityTrader;
	import pattern.OldWitch;
	import pattern.abstract.Trader;


	public class Main extends Sprite
	{
		public function Main()
		{
			var trader1:Trader = new CityTrader();
			trader1.buyItem();

			var trader2:Trader = new OldWitch();
			trader2.buyItem();
		}
	}
}
