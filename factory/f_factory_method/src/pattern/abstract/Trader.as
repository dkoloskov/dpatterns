package pattern.abstract
{
	import pattern.interfaces.IItem;


	// Abstract class
	public class Trader
	{
		public function Trader()
		{
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		// Parameters can be passed to method call
		// So it can return various objects of the same Interface/Abstract Class
		public function buyItem():void
		{
			var item:IItem = createItem();
			trace("Item bought - " + item.name);
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------

		// Factory Method
		// Abstract method. Abstract method can be implemented
		protected function createItem():IItem
		{
			return null;
		}

		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
