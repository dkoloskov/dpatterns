package
{

	import flash.display.Sprite;
	import flash.text.TextField;

	import pattern.Inventory;
	import pattern.interfaces.IIterator;

	import pattern.interfaces.ISomeObject;


	public class Main extends Sprite
	{
		public function Main()
		{
			var items:Vector.<String> = new <String>["sword", "axe", "health potion", "fireball scroll"];
			var inventory:ISomeObject = new Inventory(items);

			displayItems(inventory);
			trace("================================================");
			displayItems(inventory);
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		private function displayItems(inventory:ISomeObject):void
		{
			var itemName:String;

			// pattern in work
			var i:IIterator = inventory.createIterator();
			while(i.hasNext())
			{
				itemName = i.next();
				trace(itemName);
			}
		}

		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
