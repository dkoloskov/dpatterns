package pattern
{
	import pattern.interfaces.IIterator;
	import pattern.interfaces.ISomeObject;


	public class Inventory implements ISomeObject
	{
		private var items:Vector.<String>;

		public function Inventory(items:Vector.<String>)
		{
			this.items = items;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function createIterator():IIterator
		{
			return new Iterator(items);
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
