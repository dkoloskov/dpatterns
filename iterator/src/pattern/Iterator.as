package pattern
{
	import pattern.interfaces.IIterator;


	public class Iterator implements IIterator
	{
		private var collection:Vector.<String>;
		private var position:int;

		public function Iterator(collection:Vector.<String>)
		{
			this.collection = collection;
			position = 0;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function next():String
		{
			var currentPosition:int = position;
			position++;

			return collection[currentPosition];
		}

		public function hasNext():Boolean
		{
			if(position >= collection.length)
			{
				return false;
			}

			return true;
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
