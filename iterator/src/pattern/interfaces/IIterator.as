package pattern.interfaces
{
	public interface IIterator
	{
		//--------------------------------------------------------------------------
		//   							METHODS
		//--------------------------------------------------------------------------
		// Return parameter - is a type of items, that collection (array) contains
		function next():String;

		function hasNext():Boolean;

		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
