package pattern.interfaces
{
	// This interface is not part of the pattern, it is just a good way to show how to work with iterator
	public interface ISomeObject
	{
		//--------------------------------------------------------------------------
		//   							METHODS
		//--------------------------------------------------------------------------
		function createIterator():IIterator;

		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
