package pattern.states
{
	import pattern.Character;
	import pattern.interfaces.ICharacterState;


	public class DeadState implements ICharacterState
	{
		private var character:Character;

		public function DeadState(character:Character)
		{
			this.character = character;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function attackCharacter():void
		{
			trace("I'm dead... Don't touch me!");
		}

		public function getReadyForBattle():void
		{
			trace("I'm dead... Can't fight...");
		}

		public function battleIsOver():void
		{
			trace("Great, but you forgot about one thing, I'M DEAD!");
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
