package pattern.states
{
	import pattern.Character;
	import pattern.interfaces.ICharacterState;


	public class BattleState implements ICharacterState
	{
		private var character:Character;

		public function BattleState(character:Character)
		{
			this.character = character;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function attackCharacter():void
		{
			trace("Miss me! Take that! 'Bam' 'Bam' 'Bam'");
		}

		public function getReadyForBattle():void
		{
			trace("I'm already ready!");
		}

		public function battleIsOver():void
		{
			trace("Phew, can rest now.");
			character.setState(character.idleState);
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
