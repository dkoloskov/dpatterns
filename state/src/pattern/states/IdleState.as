package pattern.states
{
	import pattern.Character;
	import pattern.interfaces.ICharacterState;


	public class IdleState implements ICharacterState
	{
		private var character:Character;

		public function IdleState(character:Character)
		{
			this.character = character;
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function attackCharacter():void
		{
			trace("Aaaaaaaa gh ph...");
			character.setState(character.deadState);
		}

		public function getReadyForBattle():void
		{
			trace("I'm ready for battle!");
			character.setState(character.battleState);
		}

		public function battleIsOver():void
		{
			trace("What battle? Everything is quiet.");
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------

	}
}
