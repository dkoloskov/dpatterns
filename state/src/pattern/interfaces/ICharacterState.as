package pattern.interfaces
{
	public interface ICharacterState
	{
		//--------------------------------------------------------------------------
		//   							METHODS
		//--------------------------------------------------------------------------
		function attackCharacter():void;

		function getReadyForBattle():void;

		function battleIsOver():void;

		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
