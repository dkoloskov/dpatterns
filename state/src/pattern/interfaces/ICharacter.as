package pattern.interfaces
{
	// This is not part of the State pattern,
	// but this is a good way to encapsulate logic of setState in context (in Character class),
	// so, client won't be able to set context state (Character state).
	public interface ICharacter
	{
		//--------------------------------------------------------------------------
		//   							METHODS
		//--------------------------------------------------------------------------
		function attackCharacter():void;

		function getReadyForBattle():void;

		function battleIsOver():void;

		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
	}
}
