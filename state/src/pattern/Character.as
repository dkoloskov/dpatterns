package pattern
{
	import pattern.interfaces.ICharacter;
	import pattern.interfaces.ICharacterState;
	import pattern.states.BattleState;
	import pattern.states.DeadState;
	import pattern.states.IdleState;


	public class Character implements ICharacter
	{
		private var _idleState:ICharacterState;
		private var _battleState:ICharacterState;
		private var _deadState:ICharacterState;

		private var _currentState:ICharacterState;

		public function Character()
		{
			_idleState = new IdleState(this);
			_battleState = new BattleState(this);
			_deadState = new DeadState(this);

			setState(idleState);
		}

		//--------------------------------------------------------------------------
		//   							PUBLIC METHODS
		//--------------------------------------------------------------------------
		public function attackCharacter():void
		{
			_currentState.attackCharacter();
		}

		public function getReadyForBattle():void
		{
			_currentState.getReadyForBattle();
		}

		public function battleIsOver():void
		{
			_currentState.battleIsOver();
		}

		public function setState(state:ICharacterState):void
		{
			_currentState = state;
		}

		//--------------------------------------------------------------------------
		//   					  PRIVATE\PROTECTED METHODS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//   							HANDLERS
		//--------------------------------------------------------------------------
		//--------------------------------------------------------------------------
		//  							GETTERS/SETTERS
		//--------------------------------------------------------------------------
		public function get idleState():ICharacterState
		{
			return _idleState;
		}

		public function get battleState():ICharacterState
		{
			return _battleState;
		}

		public function get deadState():ICharacterState
		{
			return _deadState;
		}
	}
}
