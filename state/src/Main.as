package
{
	import flash.display.Sprite;

	import pattern.Character;

	import pattern.interfaces.ICharacter;


	public class Main extends Sprite
	{
		public function Main()
		{
			var character:ICharacter = new Character();

			// Idle state
			character.battleIsOver();
			character.getReadyForBattle();
			trace("=========================================");

			// Battle state
			character.getReadyForBattle();
			character.attackCharacter();
			character.battleIsOver();
			trace("=========================================");

			// Idle state
			character.attackCharacter();
			trace("=========================================");

			// Dead state
			character.getReadyForBattle();
			character.attackCharacter();
			character.battleIsOver();
		}
	}
}
